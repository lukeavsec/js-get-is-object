export default x => (typeof x === 'function' || typeof x === 'object') && x !== null
